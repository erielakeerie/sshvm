TODO:

1. Make configs use yaml files and integrate into code. Note below that if no `name` is given, then `username` will be assumed to be `name` in implementation. Example structure:
```yaml
- IP_PREFIX: '192.168.122'
- ip_addresses:
  - name: college
    ip_suffix: 104
    username: 'user'
  - name: work
    ip_suffix: 217
```
2. Port to python3 for portability. Rather not work with the hassle of posix-compliant gnu bash.
        
